var form = document.getElementById('addForm');
var items = document.getElementById('items');
var filter = document.getElementById('filter');

//form submit event
form.addEventListener('submit', addItem);

//delete event
items.addEventListener('click', removeItem);

//keyup event
filter.addEventListener('keyup', filterItems);

//add an item
function addItem(e){
	e.preventDefault();
	
	var inputItem = document.getElementById('item').value;
	
	// crete li element
	var li = document.createElement('li');
	li.className = "list-group-item";
	li.appendChild(document.createTextNode(inputItem));

	//append li
	items.appendChild(li);

	//create deletebutton
	var deleteBtn = document.createElement('button');
	deleteBtn.className = 'btn btn-danger btn-sm float-right delete';
	deleteBtn.appendChild(document.createTextNode('X'));
	li.appendChild(deleteBtn);
};

//remove an item
function removeItem(e){
	if(e.target.className.indexOf('delete') != -1){
		if(confirm('Are you sure?')){
			var li = e.target.parentElement;
			items.removeChild(li);
		}
	}
};

//filter items
function filterItems(e){
	var searchText = e.target.value.toLowerCase();
	Array.from(items.children).forEach(function(item){
		var itemName = item.firstChild.textContent;
		if(itemName.toLowerCase().indexOf(searchText) != -1)	{
			item.style.display = 'block';
		}else{
			item.style.display = 'none';
		}
	});

};

